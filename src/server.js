const app = require("express")(); // HTTP/API middleware
const http = require("http").Server(app); // HTTP server
const io = require("socket.io")(http); // websocket layer

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

const players = new Map();

io.on("connection", socket => {
  console.log("a user connected");

  //   socket.on("disconnect", () => {
  //     const player = players.get(socket.id);
  //     // notifikuj hracov ze sa pripojil dalsi hrac s danym menom
  //     console.log(`user '${player.id}' disconnected`);
  //     socket.emit("playerDisconnected", player.id);
  //     players.delete(socket.id);
  //   });

  socket.on("join", player => {
    console.log(`player joined`, player);
    players.set(socket.id, player);
    io.emit("playerConnected", player);
  });

  socket.on("positionChanged", position => {
    const player = players.get(socket.id);
    console.log(`user moved`, player, position);

    io.emit("playerMoved", {
      playerId: player.id,
      x: position.x,
      y: position.y
    });
  });
});

http.listen(3000, () => {
  console.log("listening on *:3000");
});
